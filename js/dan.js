$ = jQuery;
$(document).ready(function()
{
	//smooth animate scroll to all anchor links
	$("a[href='#merch'], a[href='#press'], a[href='#contact_dan']").click(function()
	{
		$("body").animate({ scrollTop: $($(this).attr('href')).offset().top }, 666);
		
		return false;
	});
	
	$(".gform_body label").each(function(){
    	$("#" + $(this).attr("for")).attr("placeholder",$(this).text());
    	$(this).remove();
	});
	
    setTimeout(function(){
    	$.ajax({
        	url: "https://api.instagram.com/v1/users/50417061/media/recent/?client_id=ea8bd3a08d5446d8a8190c20cf7d7402",
        	type: "GET",
            dataType: "jsonp",
            cache: false,
        	success: function(data) {        	
            	var posts = data.data;
            	
            	var html = "";
            	for(var i = 0 ; i < posts.length ; i++) {
                	
                	if (i == 9) break;
                	
                	if(i % 3 == 0)
                	    html += "<div class='row'>";
                	
                	html += '<div class="col-sm-4 image text-center">';
                	    html += '<a href="' + posts[i].link + '" target="_blank">';
                            html += '<img src="' + posts[i].images.low_resolution.url + '">';
                	    html += '</a>';
                	html += '</div>';
                	
                	if(i % 3 == 2)
                	    html += "</div>";
                	
            	}
            	        	
            	$("#instagram-gallery-grid .gallery").html(html);
        	}
    	});
    },5000);

    function calcAge(dateString) {
        var birthday = +new Date(dateString);
        return~~ ((Date.now() - birthday) / (31557600000));
    }

    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    };

    if(localStorage.getItem("danUser") == "remembered"){
        console.log("remembered");
        $("#inline_content form").remove();
    }else{
        $("body").addClass('fixBody');
        // $(".inline").colorbox({inline:true, width:"770px",height:"382px",open:true,scrolling:false,overlayClose:false,maxWidth:"100%"});
        $(".inline").colorbox({inline:true, width:"770px",open:true,scrolling:false,overlayClose:false,maxWidth:"100%"});

        $(".verify").click(function(){
            var dateString = $('.year').val()+"-"+$('.month').val()+"-"+$('.day').val();
            if(calcAge(dateString) >= 18){
                $(".inline2").colorbox({inline:true, width:"770px",open:true,scrolling:false,overlayClose:false,maxWidth:"100%"});
                if($("input.remember_me").is(":checked")){
                    $("input.rem_me").val(1);
                    localStorage.setItem("danUser", "remembered");
                }
            }else{
                $("#inline_content select, #inline_content .warning").addClass('error');
            }
        });
        $(".close_popup, .close_popup_link").click(function(){
            var dateString = $('.year').val()+"-"+$('.month').val()+"-"+$('.day').val();
            if(calcAge(dateString) >= 18){
                $.colorbox.close();
                $("body").removeClass('fixBody');
                $("#inline_content2 form").remove();
            }
        });

        d = new Date();
        for(i=0;i<100;i++){
            a = parseInt(d.getFullYear())-i;
            $('select.year').append("<option value='"+a+"'>"+a+"</option>");
        }
        for(i=1;i<=31;i++){
            if(i<10){
                $('select.day').append("<option value='0"+i+"'>"+i+"</option>");
            }else{
                $('select.day').append("<option value='"+i+"'>"+i+"</option>");
            }
        }

        $('.subscribe').click(function(){
            // if( isValidEmailAddress($('#inline_content2 input[type="email"]').val() )){
            // if( checkAge() && isValidEmailAddressNew($('#inline_content input[type="email"]').val()) ){
            var age_confirm = checkAge();
            var email_confirm = isValidEmailAddressNew($('#inline_content input[type="email"]').val());
            if(age_confirm && email_confirm){
                if($("input.remember_me").is(":checked")){
                    $("input.rem_me").val(1);
                    localStorage.setItem("danUser", "remembered");
                }
                $("#mc-embedded-subscribe-form1").submit();
                $.colorbox.close();
                $("body").removeClass('fixBody');
                $("#inline_content form").remove();
            }else{
                $(".inline").colorbox({inline:true, width:"770px",open:true,scrolling:false,overlayClose:false,maxWidth:"100%"});
                console.log(age_confirm+" "+email_confirm);
            }
        });

        $(window).resize(function(){
            win_resize();
        })
    }

    function checkAge(){
        var dateString = $('.year').val()+"-"+$('.month').val()+"-"+$('.day').val();
        if(calcAge(dateString) >= 18){
            $("#inline_content select, #inline_content .warning.age_warning").removeClass('error');
            return true;
        }else{
            $("#inline_content select, #inline_content .warning.age_warning").addClass('error');
            return false;
        }
    }

    function isValidEmailAddressNew(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        // return pattern.test(emailAddress);
        if(pattern.test(emailAddress)){
            $("#inline_content .warning.email_warning").removeClass('error');
            $("#mc-embedded-subscribe-form1 input[type='email']").removeClass('error');
            $(".popup_right .mce_inline_error").hide();
            return true;
        }else{
            $("#inline_content .warning.email_warning").addClass('error');
            $("#mc-embedded-subscribe-form1 input[type='email']").addClass('error');
            $(".popup_right .mce_inline_error").show();
            return false;
        }
    }

    function win_resize(){
        if($('#cboxLoadedContent > div').attr('id') == 'inline_content'){
            $(".inline").colorbox({inline:true, width:"770px",open:true,scrolling:false,overlayClose:false,maxWidth:"100%"});
        }else if($('#cboxLoadedContent > div').attr('id') == 'inline_content2'){
            $(".inline2").colorbox({inline:true, width:"770px",open:true,scrolling:false,overlayClose:false,maxWidth:"100%"});
        }
    }

    $('select').change(function(){
        win_resize();
        var dateString = $('.year').val()+"-"+$('.month').val()+"-"+$('.day').val();
        if(calcAge(dateString) >= 18){
            $(".close_popup_link").css('visibility','visible');
        }
    })

});